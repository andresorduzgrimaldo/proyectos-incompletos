// function heredaDe(prototipoHijo, prototipoPadre) {
//   var noop = function (){};
//   noop = prototipoPadre.prototype;
//   prototipoHijo.prototype = new noop;
//   prototipoHijo.prototype.constroctur = prototipoHijo;
// }

class Persona {
   constructor(nombre, apellido, altura) {
      // console.log(`Me ejecutaron`);
      this.nombre = nombre;
      this.apellido = apellido;
      this.altura = altura;
   }

   saludar(fn) {
      var {nombre, apellido } = this
      // var nombre = this.nombre
      // var apellido = this.apellido
      console.log(`Hola, me llamo ${nombre} ${apellido}`);
      if (fn) {
         fn(nombre,apellido)
      }
   }

   soyAlto() {
      return this.altura > 1.7;
   }
}

// debe utilizar el constructor de la clase padre primero con sus valores
class Desarrollador extends Persona {
   constructor(nombre, apellido, altura) {
      super(nombre, apellido, altura);
   }

   saludar(fn) {
      var { nombre, apellido} = this
      console.log(`Hola, me llamo ${nombre} ${apellido} y soy desarrollador/a`);
      if (fn) {
         fn(nombre, apellido, true)
      }
   }
}

function responderSaludo(nombre, apellido, esDev) {
   console.log(`Buen día ${nombre} ${apellido}`)
   if (esDev) {
      console.log(`Ah mirá, no sabía que eras Desarrollador`)
   }
}

// heredaDe(Desarrollador, Persona);


var andres = new Desarrollador("Andres", "Orduz Grimaldo", 1.80);
var daniela = new Persona("Daniela", "Ramirez", 1.65);
var darwin = new Persona("Darwin", "Araque", 1.70);

andres.saludar(responderSaludo);
daniela.saludar();
darwin.saludar(responderSaludo);