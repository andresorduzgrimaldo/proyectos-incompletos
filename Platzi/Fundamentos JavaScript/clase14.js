var andres = {
  nombre: "Andres",
  apellido: "Orduz Grimaldo",
  edad: 28,
  peso: 75
};

console.log(`Al inicio de año ${andres.nombre} pesa ${andres.peso}kg`);

const INCREMENTO_PESO = 0.2;
const DIAS_DEL_AÑO = 365;

// function aumentarDePeso (persona){
//     return persona.peso += 200;
// }
const aumentarDePeso = persona => (persona.peso += INCREMENTO_PESO);
const adelgazar = persona => (persona.peso -= INCREMENTO_PESO);
const comeMucho = () => Math.random() < 0.3;
const realizaDeporte = () => Math.random() < 0.4;
const META = andres.peso - 3;
var dias = 0;
while (andres.peso > META) {
  // DEBUGEA EL CODIGO
  // debugger
  if (comeMucho()) {
    // aumentar de peso
    aumentarDePeso(andres);
  }
  if (realizaDeporte) {
    // adelgazar
    adelgazar(andres);
  }
  dias += 1;
}

console.log(`Pasaron ${dias} dias hasta que ${andres.nombre} adelgazo ${andres.peso}kg`);

// for (var i = 1; i <= DIAS_DEL_AÑO; i++) {
//   var random = Math.random();
//   if (random < 0.25) {
//     // aumenta de peso
//     aumentarDePeso(andres);
//   } else if (random < 0.5) {
//     // adelgaza
//     adelgazar(andres);
//   }
// }
// console.log(`Al final del año ${andres.nombre} pesa ${andres.peso.toFixed(2)}kg `);