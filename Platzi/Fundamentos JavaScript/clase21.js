function Persona(nombre, apellido, altura){
  // console.log(`Me ejecutaron`);
  this.nombre = nombre;
  this.apellido = apellido;
  this.altura = altura;
  // no es necesario usar return, porque es implicito
  // return this;
}
// dentro del prototipo persona va a existir la funcion saludar
Persona.prototype.saludar = function(){
  console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`);
}

// de esta manera no sirve
// Persona.prototype.soyAlto = () => this.altura>1.75 ? console.log(`Yo ${this.nombre} soy alto`) : console.log(`Yo ${this.nombre} soy bajo`)

// esta si
Persona.prototype.soyAlto = function (){
	this.altura >= 1.8 ? console.log(`${this.nombre} es alto`) : console.log(`${this.nombre} es bajo`)
} 

var andres = new Persona("Andres","Orduz Grimaldo", 1.80);
andres.saludar();
andres.soyAlto();
var daniela = new Persona("Daniela", "Ramirez", 1.70);
daniela.saludar();
daniela.soyAlto();
var darwin = new Persona("Darwin", "Araque", 1.75);
darwin.saludar();