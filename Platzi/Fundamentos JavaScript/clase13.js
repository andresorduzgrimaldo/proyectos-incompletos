var andres = {
  nombre: "Andres",
  apellido: "Orduz Grimaldo",
  edad: 28,
  peso: 75
};

console.log(`Al inicio de año ${andres.nombre} pesa ${andres.peso}kg`);

const INCREMENTO_PESO = 0.2;
const DIAS_DEL_AÑO = 365;

// function aumentarDePeso (persona){
//     return persona.peso += 200;
// }
const aumentarDePeso = persona => (persona.peso += INCREMENTO_PESO);
const adelgazar = persona => (persona.peso -= INCREMENTO_PESO);

// const aumentarDePeso = ({peso}) => (peso += INCREMENTO_PESO)
// const adelgazar = ({peso}) => (peso -= INCREMENTO_PESO)

for (var i = 1; i <= DIAS_DEL_AÑO; i++) {
  var random = Math.random();
  if (random < 0.25) {
    // aumenta de peso
    aumentarDePeso(andres);
  } else if (random < 0.5) {
    // adelgaza
    adelgazar(andres);
  }
}
console.log(`Al final del año ${andres.nombre} pesa ${andres.peso.toFixed(2)}kg `);
