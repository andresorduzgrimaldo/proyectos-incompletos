function heredaDe(prototipoHijo, prototipoPadre) {
  var noop = function (){};
  noop = prototipoPadre.prototype;
  prototipoHijo.prototype = new noop;
  prototipoHijo.prototype.constroctur = prototipoHijo;
}

function Persona(nombre, apellido, altura) {
  // console.log(`Me ejecutaron`);
  this.nombre = nombre;
  this.apellido = apellido;
  this.altura = altura;
  return this;
}

// las funciones van antes de ejecutarlas y juntas
Persona.prototype.saludar = function () {
  console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`);
}

// con las funciones ArrowFunctions el this representa el objeto Window(objeto global del navegador)
// Persona.prototype.saludar = () =>
//   console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`);

Persona.prototype.esAlto = function () {
  return this.altura > 1.7;
}

// con las funciones ArrowFunctions el this representa el objeto Window
// Persona.prototype.esAlto = () => this.altura > 1.7;

function Desarrollador(nombre, apellido) {
  this.nombre = nombre;
  this.apellido = apellido;
}
heredaDe(Desarrollador, Persona);

Desarrollador.prototype.saludar = function () {
  console.log(`Hola, me llamo ${this.nombre} ${this.apellido} y soy desarrollador/a`);
}


// var andres = new Persona("Andres", "Orduz Grimaldo", 1.80);
// var daniela = new Persona("Daniela", "Ramirez", 1.65);
// var darwin = new Persona("Darwin", "Araque", 1.70);

// andres.saludar();
// daniela.saludar();
// darwin.saludar();