const MAYORIA_DE_EDAD = 18;

var andres = {
    nombre: 'Andres',
    apellido: 'Orduz Grimaldo',
    edad: 28,
    ingeniero: true,
    cocinero: true,
    cantante: false,
    dj: false,
    guitarrista: false,
    drone: true
};

var juan = {
    nombre: 'Juan',
    apellido: 'Martinez',
    edad: 17,
    ingeniero: true,
    cocinero: true,
    cantante: false,
    dj: false,
    guitarrista: false,
    drone: true
};

function imprimirProfesiones(persona) {
    console.log(`${persona.nombre} es:`);
    if (persona.ingeniero === true) {
        console.log('Ingeniero');
    } else {
        console.log('No es ingeniero');
    }

    if (persona.cocinero) {
        console.log('Cocinero');
    }
    if (persona.dj) {
        console.log('dj');
    }
    if (persona.cantante) {
        console.log('cantante');
    }
    if (persona.guitarrista) {
        console.log('guitarrista');
    }
    if(persona.drone){
        console.log('Piloto dorne');
    }
}

function imprimirSiEsMayorDeEdad(persona) {
    if (esMayorDeEdad(persona)) {
        console.log(`${persona.nombre} es mayor de edad`);

    } else {
        console.log(`${persona.nombre} es menor de edad`)
    }
}

function esMayorDeEdad(persona) {
    return persona.edad >= mayoriaDeEdad;
}

imprimirProfesiones(andres);
imprimirSiEsMayorDeEdad(andres);
imprimirSiEsMayorDeEdad(juan);