// var nombre = 'Andres';
// var nombre2 = 'Absalon';
// var nombre3 = 'Angelica';

var andres = {
   nombre: 'Andres',
   apellido: 'Orduz',
   edad: 28,
};

var absalon = {
   nombre: 'Absalon',
   apellido: 'Vergara',
   edad: 27
}

function imprimirNombreEnMayusculas(persona) {
   // var nombre = persona.nombre.toUpperCase();
   // var nombre = persona.nombre;
   var { nombre } = persona;
   console.log(nombre.toUpperCase());
}

function imprimirNombreYEdad(persona) {
   console.log('Hola, me llamo ' + persona.nombre + ' y tengo ' + persona.edad + ' años');
}

function cumpleaños(persona) {
   return {
      ...persona,
      edad: persona.edad +1
         }
}

imprimirNombreEnMayusculas(andres);
imprimirNombreEnMayusculas(absalon);
imprimirNombreEnMayusculas({ nombre: 'Pedro' });
cumpleaños(absalon);
imprimirNombreYEdad(andres);
// imprimirNombreEnMayusculas({apellido: 'Gomez'});
