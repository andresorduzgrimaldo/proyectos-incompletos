const MAYORIA_DE_EDAD = 18;

var andres = {
    nombre: 'Andres',
    apellido: 'Orduz Grimaldo',
    edad: 28,
    ingeniero: true,
    cocinero: true,
    cantante: false,
    dj: false,
    guitarrista: false,
    drone: true
};

var juan = {
    nombre: 'Juan',
    apellido: 'Martinez',
    edad: 17,
    ingeniero: true,
    cocinero: true,
    cantante: false,
    dj: false,
    guitarrista: false,
    drone: true
};

function imprimirProfesiones(persona) {
    console.log(`${persona.nombre} es:`);
    if (persona.ingeniero === true) {
        console.log('Ingeniero');
    } else {
        console.log('No es ingeniero');
    }

    if (persona.cocinero) {
        console.log('Cocinero');
    }
    if (persona.dj) {
        console.log('dj');
    }
    if (persona.cantante) {
        console.log('cantante');
    }
    if (persona.guitarrista) {
        console.log('guitarrista');
    }
}

function imprimirSiEsMayorDeEdad(persona) {
    if (esMayorDeEdad(persona)) {
        console.log(`${persona.nombre} es mayor de edad`);

    } else {
        console.log(`${persona.nombre} es menor de edad`)
    }
}

// var esMayorDeEdad = function (persona) {
//     return persona.edad >= MAYORIA_DE_EDAD;
// }

// const esMayorDeEdad = (persona) => {
//     return persona.edad >= MAYORIA_DE_EDAD;
// }

// const esMayorDeEdad = persona => {
//     return persona.edad >= MAYORIA_DE_EDAD;
// }

// const esMayorDeEdad = persona => persona.edad >= MAYORIA_DE_EDAD;

const esMayorDeEdad = ({edad}) => edad >= MAYORIA_DE_EDAD;

// function permitirAcceso(persona){
//     if(!esMayorDeEdad(persona)){
//         console.log('ACCESO DENEGADO');
//     }
// }

const permitirAcceso = persona => !esMayorDeEdad(persona);

imprimirProfesiones(andres);
imprimirSiEsMayorDeEdad(andres);
imprimirSiEsMayorDeEdad(juan);
permitirAcceso(andres);
permitirAcceso(juan);