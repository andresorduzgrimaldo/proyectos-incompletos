var x = 4, y = '4'

// comparar con x == y
// comparar si tiene el mismo valor y el mismo tipo de variable x === y

var sacha = {
   nombre : 'Sacha'
}

var otraPersona = {
   nombre: 'Sacha'
}

// crea un nuevo objeto y lo coloca en otra referencia de memoria
var otraPersona2 = {
   ...sacha
}

// De esta manera la comparacion === dara true
// si cambias a sacha, cambias a otra persona tambien
otraPersona = sacha;

// con doble igual == compara los valores no el tipo de dato
// con triple igual === compara los valores y el tipo de dato
// siempre que podamos usamos el triple igual ===

// al comparar objetos con el doble igual == compara la referencia en la memoria


