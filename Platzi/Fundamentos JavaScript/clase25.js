// function heredaDe(prototipoHijo, prototipoPadre) {
//   var noop = function (){};
//   noop = prototipoPadre.prototype;
//   prototipoHijo.prototype = new noop;
//   prototipoHijo.prototype.constroctur = prototipoHijo;
// }

class Persona {
  constructor(nombre, apellido, altura) {
    // console.log(`Me ejecutaron`);
    this.nombre = nombre;
    this.apellido = apellido;
    this.altura = altura;
  }

  saludar() {
    console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`);
  }

  soyAlto() {
    return this.altura > 1.7;
  }
}

// debe utilizar el constructor de la clase padre primero con sus valores
class Desarrollador extends Persona {
  constructor(nombre, apellido, altura) {
    super(nombre, apellido, altura);
  }

  saludar() {
    console.log(`Hola, me llamo ${this.nombre} ${this.apellido} y soy desarrollador/a`);
  }
}

// heredaDe(Desarrollador, Persona);


// var andres = new Persona("Andres", "Orduz Grimaldo", 1.80);
// var daniela = new Persona("Daniela", "Ramirez", 1.65);
// var darwin = new Persona("Darwin", "Araque", 1.70);

// andres.saludar();
// daniela.saludar();
// darwin.saludar();
