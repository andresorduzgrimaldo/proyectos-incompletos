function Persona(nombre, apellido, altura) {
  // console.log(`Me ejecutaron`);
  this.nombre = nombre;
  this.apellido = apellido;
  this.altura = altura;
  
}

// las funciones van antes de ejecutarlas y juntas
Persona.prototype.saludar = function () {
  console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`);
}

// En las arrowFunction no se ejecutan bien 
// Persona.prototype.saludar = () =>
//   console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`);

Persona.prototype.esAlto = function (){
  return this.altura > 1.7;
}
// Persona.prototype.esAlto = () => this.altura > 1.7;


var andres = new Persona("Andres", "Orduz Grimaldo", 1.80);
console.log(andres);
andres.esAlto()
var daniela = new Persona("Daniela", "Ramirez", 1.65);
var darwin = new Persona("Darwin", "Araque", 1.70);


// andres.saludar();
// daniela.saludar();
// darwin.saludar();