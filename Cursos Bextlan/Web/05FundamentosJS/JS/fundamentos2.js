//DECLARACION DE OBJETOS Y VERIABLES
var boton = document.getElementoById("boton");
var boton2 = document.getElementById("boton2");
var hola = document.getElementById("hola");

var numero = document.getElementById("numero");
var dia = fecha.getDay();
var fecha = new Date();
var hora = fecha.getHours();
var minuto = fecha.getMinutes();
var segundo = fecha.getSeconds();

var bisiesto = document.getElementById("bisiesto");

var btnReloj = document.getElementById("reloj");
var detenerReloj = document.getElementById("detener-rejol");
var muestroHora = document.getElementById("muestro-hora");
var btnAlarma = document.getElementById("alarma");
var detenerAlarma = document.getElementById("detener-alarma");

//DECLARACIÓN DE FUNCIONES
function eventoClick(e) {
  alert("Has presionado el botón");
  alert(
    "Has presionado el botón en evento '" +
      e.type +
      "' Con el objeto id '" +
      e.target.id +
      "'"
  );
  console.log(e);

  evento.target.style.borderRadius = "1em";
  evento.target.style.fontSize = "2em";

  boton2.removeEventListener("click", eventoClick);
  boton2.addEventListener("dblclick", otroEventoClick);
}

function otroEventoClick(e) {
  alert(
    "Has presionado el botón en evento '" +
      e.type +
      "' Con el objeto id '" +
      e.target.id +
      "'"
  );
  console.log(e);

  evento.target.style.background = "black";
  evento.target.style.color = "white";
}

function parImpar() {
  var numero = prompt("Ingresa un número");
  //isNaN (isNotNumber) true=alfanumérico, false=numerico
  if (isNaN(numero)) {
    alert("No me engañes, eso no es un numero");
  } else {
    // alert("Es un numero");
    var modulo = numero % 2;
    var tipo = modulo == 0 ? "Par" : "Impar";
    var tipo = modulo == 1 ? "Impar" : "Par";
    alert("El numero " + numero + " es  " + tipo);
  }
}
function saluda() {
  // alert(fecha+" "+hora);

  var horaCSS = document.createElement("link");
  horaCSS.rel = "stylesheet";

  if (hora < 6) {
    alert("Deja dormir");
  } else if (hora > 5 && hora < 12) {
    alert("Buenos dias");
    hojaCSS.href = "activos/dia.css";
  } else if (hora > 12 && hora <= 18) {
    alert("Buenas tardes");
    hojaCSS.href = "activos/tarde.css";
  } else {
    alert("Buenas noches");
    hojaCSS.href = "activos/noche.css";
  }
  document.head.appendChild(hojaCSS);

  alert(dia);
  /*
         D L M Mi J V S
         0 1 2 3  4 5 6
      */

  switch (dia) {
    case 0:
      alert("Es Domingo");
      breack;
    case 1:
      alert("Es Lunes");
      breack;
    case 2:
      alert("Es Martes");
      breack;
    case 3:
      alert("Es Miercoles");
      breack;
    case 4:
      alert("Es Jueves");
      breack;
    case 5:
      alert("Es Viernes");
      breack;
    case 6:
      alert("Es Sabado");
      breack;
      defalut: alert("Estoy esperando");
  }
}

function anioBisiesto() {
  var anio = prompt("Ingresa un año");
  if (isNaN(anio)) {
    alert("No es un año");
  } else {
    if ((anio % 4 == 0 && anio % 100 != 0) || anio % 400 == 0) {
      alert("El año " + anio + " es bisiesto");
    } else {
      alert("El año " + anio + " no es bisiesto");
    }
  }
}

function reloj() {
  //  muestroHora.innerHTML = new Date();
  var fecha = new Date();
  var hora = fecha.getHours();
  var minuto = fecha.getMinutes();
  var segundo = fecha.getSeconds();

  if (hora <= 9) {
    hora = "0" + hora;
  }
  if (minuto <= 9) {
    minuto = "0" + minuto;
  }
  if (segundo <= 9) {
    segundo = "0" + segundo;
  }

  muestroHora.innerHTML =
    "<h3>" + hora + ":" + minuto + ":" + segundo + "</h3>";
}

function alarma(){
  var audio = document.createElement("audio");
  audio.src = "activos/alarma.mp3";
  return audio.play();
}

// ASIGNACIÓN DE EVENTOS
// los manejadores de eventos semánticos se ejecuta a la carga del documento
// window.onload = eventoClick;
window.onload = function() {
  boton.onclick = eventoClick;
  boton.onclick = otroEventoClick;

  boton2.addEventListener("click", eventoClick);
  numero.addEventListener("click", parImpar);
  hola.addEventListener("clic", saluda);
  bisiesto.addEventListener("clic", anioBisiesto);

  btnReloj.addEventListener("click", function() {
    // setInterval(reloj, 1000);
    iniciarReloj = setInterval(reloj, 1000);
  });

  detenerReloj.addEventListener("click", function() {
    clearInterval(iniciarReloj);
  });

  btnAlarma.addEventListener("click", function() {
    iniciarAlarma = setTimeout(alarma, 3000);
  });

  detenerAlarma.addEventListener("click", function(){
    clearTimeout(iniciarAlarma);
  });
};
