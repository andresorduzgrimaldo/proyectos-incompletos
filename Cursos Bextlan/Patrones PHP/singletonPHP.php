<?php
class PatronSingleton{
 
    // Contenedor de la instancia del singleton
    private static $instancia;
 private $usuarios = array();
 private $dbh;
 
    // Un constructor privado evita la creación de un nuevo objeto
    private function __construct() {
 $this->dbh = new PDO("mysql:host=server;dbname=database", "user", "password");
    }
 
    // método singleton
    public static function singleton()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        } 
        return self::$instancia;
    }
 
 public function usuarios()
 {
 $consulta = $this->dbh->prepare("SELECT * FROM users");
 $consulta->execute();
 if ($consulta->rowCount()>0) 
 {
            while($reg = $consulta->fetch())
            {
               $this->usuarios[]=$reg;
        	}
            return $this->usuarios;     
        }
 }
   
    // Evita que el objeto se pueda clonar
    public function __clone()
    {
        trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
    }
}
?>

<?php
//hacemos un require_once del archivo que contiene nuestra clase
require_once 'patronsingleton.php';
//accedemos al método singleton que es quién crea la instancia
//de nuestra clase y así podemos acceder sin necesidad de 
//crear nuevas instancias, lo que ahorra consumo de recursos
$nuevoSingleton = PatronSingleton::singleton();
//accedemos al método usuarios y los mostramos
$usuario = $nuevoSingleton->usuarios();
?>
Mostramos a todos los usuarios<br />
<?php
foreach ($usuario as $fila):?>
    <?=$fila['name']?> || <?=$fila['email']?><br /> 
<?php
endforeach;
?>