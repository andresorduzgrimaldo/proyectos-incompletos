// JavaScript Document

window.addEventListener('load', iniciar, false);

function iniciar() {
	boton.addEventListener('click', GestionarSueldo);
}

function GestionarSueldo() {
	var sueldo = prompt('Dinos el sueldo que tienes');
	var aumento = prompt('Dinos el aumento que te van a aplicar');
	var num_meses = prompt('Dinos el número de nóminas anual');
	SubirSueldo(sueldo, aumento, num_meses);
}

function SubirSueldo(sueldoRecibido, aumentoRecibido, num_mesesRecibido) {
	var nuevoSueldo = sueldoRecibido * aumentoRecibido;
	document.write('El nuevo sueldo es de ' + nuevoSueldo + '€');
	document.write('<br/>');
	document.write('Vas a cobrar cada mes ' + nuevoSueldo / num_mesesRecibido + '€');
}