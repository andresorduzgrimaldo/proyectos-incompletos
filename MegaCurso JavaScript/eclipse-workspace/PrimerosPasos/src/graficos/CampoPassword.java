package graficos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class CampoPassword {

	public static void main(String[] args) {
		MarcoPassword miMarco = new MarcoPassword();
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoPassword extends JFrame{
	public MarcoPassword() {
		setBounds(400,300,550,400);
		LaminaPassword miLamina = new LaminaPassword();
		add(miLamina);
		setVisible(true);
	}
}

class LaminaPassword extends JPanel{
	
	JPasswordField CContra;
	
	public LaminaPassword() {
		setLayout(new BorderLayout());
		JPanel laminaSuperior = new JPanel();
		laminaSuperior.setLayout(new GridLayout(2,2));
		add(laminaSuperior, BorderLayout.NORTH);
		JLabel etiqueta1 = new JLabel("Usuario");
		JLabel etiqueta2 = new JLabel("Contraseņa");
		JTextField CUsuario = new JTextField(15);
		CContra = new JPasswordField(15);
		CompruebaPass miEvento = new CompruebaPass();
		CContra.getDocument().addDocumentListener(miEvento);
		laminaSuperior.add(etiqueta1);
		laminaSuperior.add(CUsuario);
		laminaSuperior.add(etiqueta2);
		laminaSuperior.add(CContra);
		JButton enviar = new JButton("Enviar");
		add(enviar, BorderLayout.SOUTH);
	}
	private class CompruebaPass implements DocumentListener{

		@Override
		public void changedUpdate(DocumentEvent e) {
			
		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			char[] contrasenia;
			contrasenia = CContra.getPassword();
			if(contrasenia.length<8 || contrasenia.length>12) {
				CContra.setBackground(Color.red);
			}else {
				CContra.setBackground(Color.white);
			}
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			char[] contrasenia;
			contrasenia = CContra.getPassword();
			if(contrasenia.length<8 || contrasenia.length>12) {
				CContra.setBackground(Color.red);
			}else {
				CContra.setBackground(Color.white);
			}
		}
	}
}
