package graficos;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class TrabajandoConFuentes {
	public static void main(String[] args) {
		MarcoConImagen miMarco = new MarcoConImagen();
		miMarco.setVisible(true);
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}

class MarcoConImagen extends JFrame{
	public MarcoConImagen() {
		setTitle("prueba con Imagen");
		setSize(400,400);
		LaminaConImagen miLamina = new LaminaConImagen();
		add(miLamina);
		miLamina.setForeground(Color.blue);
	}
	
}

class LaminaConImagen extends JPanel{
	public LaminaConImagen() {
		File miImagen = new File("src/graficos/bola.png");
		try {
			imagen = ImageIO.read(miImagen);
		}catch(IOException e) {
			System.out.println("La imagen no se encuentra");
		}
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int anchuraImagen = imagen.getWidth(this);
		int alturaImagen = imagen.getHeight(this);
		g.drawImage(imagen, 0, 0, null);
		for(int i=0; i<300;i++) {
			for(int j=0; j<300; j++) {
				if(i+j>0) {
					g.copyArea(0, 0, anchuraImagen, alturaImagen, anchuraImagen*i, alturaImagen*j);
				}
			}
		}
		
	}
	private Image imagen;
}
