package graficos;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextPane;

public class Procesador3 {
	public static void main(String[] args) {
		MenuProcesador3 miMarco = new MenuProcesador3();
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}

class MenuProcesador3 extends JFrame{
	public MenuProcesador3() {
		setBounds(500,300,550,400);
		LaminaProcesador2 miLamina = new LaminaProcesador2();
		add(miLamina);
		setVisible(true);
	}
}

class LaminaProcesador3 extends JPanel{
	
	JTextPane miArea;
	JMenu fuente, estilo, tamanio;
	Font letras;
	
	public LaminaProcesador3() {
		setLayout(new BorderLayout());
		JPanel laminaMenu = new JPanel();
		JMenuBar miBarra = new JMenuBar();
		
		fuente = new JMenu("Fuente");
		estilo = new JMenu("Estilo");
		tamanio = new JMenu("Tama�o");
		
		configuraMenu("Arial", "fuente", "Arial", 9, 10);
		configuraMenu("Courier", "fuente", "Courier", 9, 10);
		configuraMenu("Verdana", "fuente", "Verdana", 9, 10);
		
		configuraMenu("Negrita", "estilo", "", Font.BOLD, 1);
		configuraMenu("Cursiva", "estilo", "", Font.ITALIC, 1);
		
		configuraMenu("12", "tamanio", "", 9, 12);
		configuraMenu("16", "tamanio", "", 9, 16);
		configuraMenu("20", "tamanio", "", 9, 20);
		configuraMenu("24", "tamanio", "", 9, 24);
		/*
		JMenuItem arial = new JMenuItem("Arial");
		JMenuItem courier = new JMenuItem("Corurier");
		JMenuItem verdana = new JMenuItem("Verdana");
		//GestionaMenus tipoLetra = new GestionaMenus();
		//courier.addActionListener(new GestionaMenus());
		courier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				miArea.setFont(new Font("Courier", Font.PLAIN, 12 ));
			}
		});
		arial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				miArea.setFont(new Font("Arial", Font.PLAIN, 12 ));
			}
		});
		verdana.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				miArea.setFont(new Font("Verdanar", Font.PLAIN, 12 ));
			}
		});
		
		
		fuente.add(arial);
		fuente.add(courier);
		fuente.add(verdana);
		
		JMenuItem negrita = new JMenuItem("Negrita");
		JMenuItem cursiva = new JMenuItem("cursiva");
		
		estilo.add(negrita);
		estilo.add(cursiva);
		
		JMenuItem tam12 = new JMenuItem("12");
		JMenuItem tam16 = new JMenuItem("16");
		JMenuItem tam20 = new JMenuItem("20");
		JMenuItem tam24 = new JMenuItem("24");
		
		tamanio.add(tam12);
		tamanio.add(tam16);
		tamanio.add(tam20);
		tamanio.add(tam24);
		*/
		miBarra.add(fuente);
		miBarra.add(estilo);
		miBarra.add(tamanio);
		laminaMenu.add(miBarra);
		add(laminaMenu, BorderLayout.NORTH);
		miArea = new JTextPane();
		add(miArea, BorderLayout.CENTER);
	}
	
	public void configuraMenu(String rotulo, String menu, String tipoLetra, int estilos, int tam) {
		JMenuItem elemMenu = new JMenuItem(rotulo);
		if(menu=="fuente") {
			fuente.add(elemMenu);
		}else if(menu=="estilo") {
			estilo.add(elemMenu);
		}else if(menu=="tamanio") {
			tamanio.add(elemMenu);
		}
		elemMenu.addActionListener(new GestionaEventos(rotulo, tipoLetra, estilos, tam));
	}
	
	private class GestionaEventos implements ActionListener{
		
		String tipoTexto, menu;
		int estiloLetra, tamanioLetra;
		GestionaEventos(String elemento, String texto2, int estilo2, int tamLetra){
			tipoTexto = texto2;
			estiloLetra = estilo2;
			tamanioLetra = tamLetra;
			menu = elemento;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			letras = miArea.getFont();
			if(menu=="Arial" || menu=="Courier" || menu=="Verdana") {
				estiloLetra=letras.getStyle();
				tamanioLetra=letras.getSize();
			}else if(menu=="Cursiva" || menu=="Negrita") {
				if(letras.getStyle()==1 || letras.getStyle()==2) {
					estiloLetra=3;
				}
				tipoTexto = letras.getFontName();
				tamanioLetra=letras.getSize();
			}else if(menu=="12" || menu=="16"||menu=="20"||menu=="24"){
				estiloLetra = letras.getStyle();
				tipoTexto=letras.getFontName();
			}
			miArea.setFont(new Font(tipoTexto, estiloLetra, tamanioLetra));
			System.out.println("Tipo: "+tipoTexto+ " Estilo: "+estiloLetra+" Tama�o: "+tamanioLetra);
		}
	}
	
	private class GestionaMenus implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			miArea.setFont(new Font("Courier", Font.PLAIN, 12 ));
		}
	}
}