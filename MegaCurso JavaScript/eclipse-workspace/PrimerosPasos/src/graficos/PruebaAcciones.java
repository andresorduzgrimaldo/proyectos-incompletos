package graficos;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class PruebaAcciones {

	public static void main(String[] args) {
		MarcoAccion marco = new MarcoAccion();
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		marco.setVisible(true);
	}
}

class MarcoAccion extends JFrame{
	public MarcoAccion() {
		setTitle("Prueba Acciones");
		setBounds(600,350,600,300);
		PanelAccion lamina = new PanelAccion();
		add(lamina);
	}
}

class PanelAccion extends JPanel{
	public PanelAccion() {
		
		AccionColor accionAmarillo = new AccionColor("Amarillo", new ImageIcon("src/graficos/amarillo.png"), Color.yellow);
		AccionColor accionAzul = new AccionColor("Azul", new ImageIcon("src/graficos/azul.png"), Color.blue);
		AccionColor accionRojo = new AccionColor("Rojo", new ImageIcon("src/graficos/rojo.png"), Color.red);
		
		JButton botonAmarillo = new JButton(accionAmarillo);
		add(botonAmarillo);
		JButton botonAzul = new JButton(accionAzul);
		add(botonAzul);
		JButton botonRojo = new JButton(accionRojo);
		add(botonRojo);
		/*
		JButton botonAmarillo = new JButton("Amarillo");
		JButton botonAzul = new JButton("Azul");
		JButton botonRojo = new JButton("Rojo");
		add(botonAmarillo);
		add(botonAzul);
		add(botonRojo);
		*/
		
		InputMap mapaEntrada = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		
		KeyStroke teclaAmarillo = KeyStroke.getKeyStroke("ctrl A");
		mapaEntrada.put(teclaAmarillo, "fondoAmarillo");
		KeyStroke teclaAzul = KeyStroke.getKeyStroke("ctrl B");
		mapaEntrada.put(teclaAzul, "fondoAzul");
		KeyStroke teclaRojo = KeyStroke.getKeyStroke("ctrl C");
		mapaEntrada.put(teclaRojo, "fondoRojo");
		
		ActionMap mapaAccion = getActionMap();
		mapaAccion.put("fondoAmarillo", accionAmarillo);
		mapaAccion.put("fondoAzul", accionAzul);
		mapaAccion.put("fondoRojo", accionRojo);
	}
	
	private class AccionColor extends AbstractAction{
		
		public AccionColor(String nombre, Icon icono, Color colorBoton) {
			putValue(Action.NAME, nombre);
			putValue(Action.SMALL_ICON, icono);
			putValue(Action.SHORT_DESCRIPTION, "Poner la lamina de color "+nombre);
			putValue("colorDeFondo", colorBoton);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Color c = (Color)getValue("colorDeFondo");
			setBackground(c);
			System.out.println("Nombre: "+getValue(Action.NAME)+" Descripción: "+getValue(Action.SHORT_DESCRIPTION));
		}
	}
}

