package graficos;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;

public class EventoRaton {

	public static void main(String[] args) {
		MarcoRaton miMarco = new MarcoRaton();
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}

class MarcoRaton extends JFrame{
	public MarcoRaton() {
		setVisible(true);
		setBounds(700,300,600,450);
		EventosDeRaton eventoRaton = new EventosDeRaton();
		addMouseMotionListener(eventoRaton);
	}
}
/*
class EventosDeRaton extends MouseAdapter{
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		System.out.println("Has hecho clic");
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		System.out.println("Acabas de entrar");
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		System.out.println("Acabas de salir");
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		System.out.println("Acabas de presionar");
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		System.out.println("Acabas de levantar");
		
	}
	
	
	public void mouseClicked(MouseEvent e) {
		//System.out.println("Coordenada X: "+e.getX()+" Coordenada Y: "+e.getY());
		//System.out.println(e.getClickCount());
	}
	
	public void mousePressed(MouseEvent e) {
		//System.out.println(e.getModifiersEx());
		if(e.getModifiersEx()==MouseEvent.BUTTON2_DOWN_MASK) {
			System.out.println("Has pulsado el bot�n izquierdo");
		}else if(e.getModifiersEx()==MouseEvent.BUTTON2_DOWN_MASK) {
			System.out.println("Has pulsado la rueda del raton");
		}else if(e.getModifiersEx()==MouseEvent.BUTTON3_DOWN_MASK) {
			System.out.println("Has pulsado el bot�n derecho");
		}
	}
}
*/

class EventosDeRaton implements MouseMotionListener{

	@Override
	public void mouseDragged(MouseEvent arg0) {
		System.out.println("Est�s arrastrando");
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		System.out.println("Est�s moviendo");
		
	}
	
}
