package graficos;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MarcoSliders2 {

	public static void main(String[] args) {
		FrameSliders2 miMarco = new FrameSliders2();
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class FrameSliders2 extends JFrame{
	public FrameSliders2() {
		setBounds(550,400,550,350);
		LaminaSliders2 miLamina = new LaminaSliders2();
		add(miLamina);
		setVisible(true);
	}
}

class LaminaSliders2 extends JPanel{
	
	private JLabel rotulo;
	private JSlider control;
	private static int contador;
	
	public LaminaSliders2() {
		setLayout(new BorderLayout());
		rotulo = new JLabel("EN un lugar de la mancha de cuyo nombre...");
		add(rotulo, BorderLayout.CENTER);
		control = new JSlider(8, 50, 12);
		control.setMajorTickSpacing(20);
		control.setMinorTickSpacing(5);
		control.setPaintTicks(true);
		control.setPaintLabels(true);
		control.setFont(new Font("Serif", Font.ITALIC, 10));
		control.addChangeListener(new EventoSlider());
		JPanel laminaSlider = new JPanel();
		laminaSlider.add(control);
		add(laminaSlider, BorderLayout.NORTH);
	}
	
	private class EventoSlider implements ChangeListener{

		@Override
		public void stateChanged(ChangeEvent e) {
			//contador++;
			//System.out.println("Estas manipulando el deslizante: "`+ control.getValue());
			rotulo.setFont(new Font("Serif", Font.PLAIN, control.getValue()));
		}
		
	}
}
