import javax.swing.*;
public class UsoArrays2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String [] paises = new String [8];
		/*
		paises[0]="Colombia";
		paises[1]="Mexico";
		paises[2]="Espa�a";
		paises[3]="Peru";
		paises[4]="Chile";
		paises[5]="Argentina";
		paises[6]="Ecuador";
		paises[7]="Venezuela";
		 */
		//String [] paises= {"Colombia","Mexico","Espa�a","Peru","Chile","Argentina","Ecuador","Venezuela"};
		/*
		for(int i =0; i<paises.length;i++) {
			System.out.println("Pa�s " +(i+1)+" "+paises[i]);
		}^
		*/
		
		for(int i=0; i<paises.length;i++) {
			paises[i]=JOptionPane.showInputDialog("Introduce pa�s "+(i+1));
		}
		
		for(String pais : paises) {
			System.out.println("Pais: "+pais);
		}
	}

}
