package poo;

public class Furgoneta extends Coche{
	
	private int capacidadCarga;
	private int plazasExtra;
	
	public Furgoneta(int plazasExtras, int capacidadCarga) {
		
		super();  //Llamar al constructor de la clase padre
		this.capacidadCarga = capacidadCarga;
		this.plazasExtra = plazasExtras;
	}
	
	public String dimeDatosFurgoneta() {
		return "La capacidad de carga es: "+capacidadCarga+" Y las plazas son: "+plazasExtra;
	}

}
