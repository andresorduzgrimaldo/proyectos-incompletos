package poo;
import java.util.*;
public class UsoEmpleado {

	public static void main(String[] args) {
		
		/*
		Empleado empleado1 = new Empleado("Paco G�mez", 85000000, 1990, 12, 17);
		Empleado empleado2 = new Empleado("Ana L�pez", 95000000, 1994, 2, 7);
		Empleado empleado3 = new Empleado("Maria Mart�n", 185000000, 1999, 3, 27);
		
		empleado1.subeSueldo(5);
		empleado2.subeSueldo(5);
		empleado3.subeSueldo(5);
		System.out.println("Nombre: "+empleado1.dameNombre()+"Sueldo: "+empleado1.dameSueldo()+
				"Fecha de Alta: "+empleado1.dameFechaContrato());
		System.out.println("Nombre: "+empleado2.dameNombre()+"Sueldo: "+empleado2.dameSueldo()+
				"Fecha de Alta: "+empleado2.dameFechaContrato());
		System.out.println("Nombre: "+empleado3.dameNombre()+"Sueldo: "+empleado3.dameSueldo()+
				"Fecha de Alta: "+empleado3.dameFechaContrato());
		*/
		
		Jefatura jefe_RRHH = new Jefatura("Andres", 550000, 2006, 9,25);
		jefe_RRHH.estableceIncetivo(25700);
		
		Empleado[] misEmpleados = new Empleado[6];
		misEmpleados[0]= new Empleado("Paco G�mez", 85000000, 1990, 12, 17);
		misEmpleados[1] = new Empleado("Ana L�pez", 95000000, 1994, 2, 7);
		misEmpleados[2] = new Empleado("Maria Mart�n", 185000000, 1999, 3, 27);
		misEmpleados[3] = new Empleado("Antonio Fernandez",30000000, 2000,01,01);
		misEmpleados[4] = jefe_RRHH; //Polimorfismo en accion. Principio de sustitucion
		misEmpleados[5] = new Jefatura("Kelly Arevalo", 950000,1999,5,26);
		/*double num1 = 7.5;
		int nom2 = (int) num1;*/
		Jefatura jefa_finanzas = (Jefatura) misEmpleados[5];
		jefa_finanzas.estableceIncetivo(500000);
		/*
		Empleado directorComercial = new Jefatura("Sandra",850000,2012,05,05);
		Comparable ejemplo = new Empleado("Elizabeth",950000,2011,06,07);
		if(directorComercial instanceof Empleado) {
			System.out.println("Es de tipo Jefatura");
		}
		if(ejemplo instanceof Comparable) {
			System.out.println("Implementa la interfaz comparable");
		}
		*/
		
		//Jefatura jefe_compras=(Jefatura)misEmpleados[1]; //Da un error porque un empleado no es Jefe
		
		
		
		
		/*
		for(int i=0; i<misEmpleados.length;i++) {
			misEmpleados[i].subeSueldo(5);
			
		}
		for(int i=0; i<misEmpleados.length;i++) {
			System.out.println("Nombre: "+misEmpleados[i].dameNombre()+" Sueldo: "+misEmpleados[i].dameSueldo()+
					" Fecha de alta: "+misEmpleados[i].dameFechaContrato());
			
		}
		*/
		
		System.out.println(jefa_finanzas.tomarDecisiones("Dar mas d�as de vacaciones a los empleados"));
		
		System.out.println(misEmpleados[3].dameNombre()+" tiene un bonus de: "+misEmpleados[3].establece_bonus(2000));
		
		System.out.println("El jefe "+jefa_finanzas.dameNombre()+" tiene un bonus de "+jefa_finanzas.establece_bonus(5000)); 
		
		for(Empleado e: misEmpleados) {
			e.subeSueldo(5);
		}
		
		Arrays.sort(misEmpleados);
		
		for(Empleado e: misEmpleados) {
			System.out.println("Nombre: "+e.dameNombre()+" Sueldo: "+e.dameSueldo()+
					" Fecha de alta: "+e.dameFechaContrato());
		}
	}

}

class Empleado implements Comparable, Trabajadores{
	
	public Empleado() {
		
	}
	
	public Empleado(String nom, double sue, int anio, int mes, int dia) {
		nombre = nom;
		sueldo = sue;
		GregorianCalendar calendario = new GregorianCalendar(anio,mes-1, dia);
		altaContrato = calendario.getTime();
		++idSiguiente;
		id = idSiguiente;
	}
	
	public double establece_bonus(double gratificacion) {
		return Trabajadores.bonus_base+gratificacion;
	}
	
	public Empleado(String nom) {
		this(nom,30000000,2000,01,01);
	}
	
	public String dameNombre() { //getter
		return nombre;		
	}
	
	public double dameSueldo() { //getter
		return sueldo;
	}
	
	public Date dameFechaContrato() { //getter
		return altaContrato;
	}
	
	public void subeSueldo(double porcentaje) {
		double aumento = sueldo * porcentaje/100;
		sueldo += aumento;
	}
	
	public int compareTo(Object miObjeto) {
		Empleado otroEmpleado = (Empleado) miObjeto;
		if(this.sueldo<otroEmpleado.sueldo) {
			return -1;
		}
		if(this.sueldo > otroEmpleado.sueldo) {
			return 1;
		}
		return 0;
	}
	
	private String nombre;
	private double sueldo;
	private Date altaContrato;
	private static int idSiguiente;
	private int id;
	
	
	
}

/*final*/class Jefatura extends Empleado implements Jefes{
	
	public Jefatura(String nom, double sue, int agno, int mes, int dia) {
		super(nom, sue, agno, mes, dia);
	}
	
	public String tomarDecisiones(String decision) {
		return "Un miembro de la direcci�n ha tomado la decisi�n de: "+decision;
	}
	
	public double establece_bonus(double gratificacion) {
		double prima = 2000;
		return Trabajadores.bonus_base+gratificacion+prima;
	}
	
	public void estableceIncetivo(double b) {
		incetivo = b;
	}
	
	public double dameSueldo() {
		double sueldoJefe = super.dameSueldo();
		return sueldoJefe + incetivo;
	}
	
	private double incetivo;
}

/*
class Director extends Jefatura{
	
	public Director (String nom, double sue, int agno, int mes, int dia) {
		super(nom, sue, agno, mes, dia);
	}
}
*/
