package poo;

import java.util.Date;
import java.util.GregorianCalendar;

public class Uso_Persona {

	public static void main(String[] args) {
		Persona[] lasPersonas = new Persona[2];
		lasPersonas[0] = new Empleado2("Luis Conde",500000,2009,02,05);
		lasPersonas[1] = new Alumno("Ana Lopez", "Biol�gicas");
		
		for(Persona p: lasPersonas) {
			System.out.println(p.dameNombre()+". "+p.dameDescripcion());
		}
	}

}

abstract class Persona{
	
	private String nombre;
	
	public Persona(String nom) {
		nombre = nom;
	}
	
	public String dameNombre() {
		return nombre;
	}
	
	public abstract String dameDescripcion();
}

class Empleado2 extends Persona{
	
	private double sueldo;
	private Date altaContrato;
	private static int idSiguiente;
	private int id;
	
	public Empleado2(String nom, double sue, int anio, int mes, int dia) {
		super(nom);
		sueldo = sue;
		GregorianCalendar calendario = new GregorianCalendar(anio,mes-1, dia);
		altaContrato = calendario.getTime();
		++idSiguiente;
		id = idSiguiente;
	}
	
	public double dameSueldo() { //getter
		return sueldo;
	}
	
	public Date dameFechaContrato() { //getter
		return altaContrato;
	}
	
	public void subeSueldo(double porcentaje) {
		double aumento = sueldo * porcentaje/100;
		sueldo += aumento;
	}
	
	public String dameDescripcion() {
		return "Este empleado tiene un ID = "+id+" con un sueldo = "+sueldo;
	}
}

class Alumno extends Persona{
	
	private String carrera;
	
	public Alumno(String nom, String car) {
		super(nom);
		carrera = car;
	}
	
	public String dameDescripcion() {
		return "Este alumno est� estudiando la carrera de "+carrera;
	}
}