package poo;

public class Coche {
	private int ruedas;
	private int largo;
	private int ancho;
	private int motor;
	private int pesoPlataforma;
	private String color;
	private int pesoTotal;
	private boolean asientosCuero;
	private boolean climatizador;
	
	public Coche(){
		ruedas = 4;
		largo = 20000;
		ancho = 300;
		motor = 1600;
		pesoPlataforma = 500;
	}
	
	public String dimeDatosGenerales(){	//GETTER
		return "La plataforma del veh�culo tiene "+ruedas+" ruedas"+
				". Mide "+largo/1000+" metros con un ancho de "+ancho+
				" cm y un peso de plataforma de "+pesoPlataforma+" kg.";
	}
	
	public void estableceColor(String colorCoche) { //SETTER
		color = colorCoche;
	}
	
	public String dimeColor() {
		return "El color color del coche es: "+color;
	}
	public void configuraAsientos(String asientosCuero) {
		if(asientosCuero.equalsIgnoreCase("si")) {
			this.asientosCuero = true;
		}else {
			this.asientosCuero = false;
		}
	}
	
	public String dimeAsientos() {
		if(asientosCuero) {
			return "El coche tiene asientos de cuero";
		}else {
			return "El coche tiene asientos de serie";
		}
	}
	
	public void configuraClimatizador(String climatizador) {
		if(climatizador.equalsIgnoreCase("si")) {
			this.climatizador = true;
		}else {
			this.climatizador = false;
		}
	}
	
	public String dimeClimatizador() {
		if(climatizador) {
			return "EL coche incorpora climatizador";
		}else {
			return "El coche lleva ire acondicionado";
		}
	}
	public String dimePesoCoche() { //Setter + Getter
		int pesoCarroceria =500;
		pesoTotal = pesoPlataforma+pesoCarroceria;
		if(asientosCuero==true) {
			pesoTotal = pesoTotal +50;
		}
		if(climatizador == true) {
			pesoTotal = pesoTotal + 20;
		}
		return "El peso del coche es: "+pesoTotal;
	}
	
	public int precioCoche() { //Getter
		int precioFinal = 100000;
		if(asientosCuero) {
			precioFinal += 2000;
		}
		if(climatizador) {
			precioFinal +=1500;
		}
		return precioFinal;
	}
}
