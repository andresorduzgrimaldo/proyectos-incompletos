
public class ArrayBidimencionales2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int	[][] matrix = {
				{10,15,18,18,21},
				{5,25,37,41,15},
				{7,19,32,14,90},
				{85,2,7,40,27},
		};
		
		/*
		for(int i=0; i<matrix.length;i++) {
			for(int j=0; j<matrix[i].length;j++) {
				System.out.print(matrix[i][j]+" ");
			}
		}
		*/
		
		for(int[]fila: matrix) {
			System.out.println();
			for(int z: fila) {
				System.out.print(z+" ");
			}
		}
	}

}
