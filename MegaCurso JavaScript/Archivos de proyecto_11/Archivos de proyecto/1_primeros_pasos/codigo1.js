// JavaScript Document
window.addEventListener('load',iniciar,false);

function iniciar(){
	var boton = document.getElementById('obtener');
	boton.addEventListener('click',obtener,false);
}

function obtener(){
	navigator.geolocation.getCurrentPosition(mostrar);
}

function mostrar(posicion){
	var ubicacion=document.getElementById('ubicacion');
	var datos='';
	datos += 'Latitud: <strong>'+posicion.coords.latitude+'</strong><br/>';
	datos += 'Longitud: <strong>'+posicion.coords.longitude+'</strong><br/>';
	datos += 'Exactitud: <strong>'+posicion.coords.accuracy+'mts.</strong><br/>';
	ubicacion.innerHTML = datos;
}