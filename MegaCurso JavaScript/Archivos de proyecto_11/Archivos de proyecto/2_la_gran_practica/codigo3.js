// JavaScript Document
window.addEventListener('load',iniciar,false);

function iniciar(){
	var boton = document.getElementById('obtener');
	boton.addEventListener('click',obtener,false);
}

function obtener(){
	
	var geoconfig={
		enableHighAccuracy: true,
		timeout: 10000,
		maximumAge: 60000
	};
	
	navigator.geolocation.getCurrentPosition(mostrar, errores, geoconfig);
}

function mostrar(posicion){
	var ubicacion=document.getElementById('ubicacion');
	var datos='';
	datos += 'Latitud: <strong>'+posicion.coords.latitude+'</strong><br/>';
	datos += 'Longitud: <strong>'+posicion.coords.longitude+'</strong><br/>';
	datos += 'Exactitud: <strong>'+posicion.coords.accuracy+'mts.</strong><br/>';
	ubicacion.innerHTML = datos;
}

function errores(error){
	alert('Error: '+error.code+' - '+error.message);
}











