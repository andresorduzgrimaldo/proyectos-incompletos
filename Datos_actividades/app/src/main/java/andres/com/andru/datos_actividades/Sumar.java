package andres.com.andru.datos_actividades;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by andru on 11/11/2017.
 */

public class Sumar extends Activity{

    public void onCreate (Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.resultado);

        Bundle datos = getIntent().getExtras();
        int num1 = datos.getInt("numero1");
        int num2 = datos.getInt("numero2");
        int resultado = num1 + num2;
        TextView valor_resultado =(TextView) findViewById(R.id.texto_resultado);
        valor_resultado.setText("El resultado es: "+resultado);
    }
}
