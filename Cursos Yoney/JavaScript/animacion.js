// // DECLARACION, INICIALIZACION, UTILIZACION
// var gato_1 = "Esto es un gato";

// // DECLARACION
// var gato;
// var barco;
// var contenedor;

// // INICIALIZACION
// gato ="Esto es un gato";
// barco = 123123;
// contenedor =gato;

// // UTILIZACION
// alert(gato);
// alert(barco);

// // FUNCIONES
// funcion uno(){
//     // esta variable solo vale dentro de esta funcion 
//     var gato ="esto es un gato";
//     gato ="no es un gato";

//     // CONSTANTES
//     const PI=3.1416;

//     alert(gato);
//     alert(PI);

//     document.write(gato);
//     console.log(gato);
// }

// // TIPOS DE DATOS

// // NUMERICOS
// var primero = 12345678;
// document.write(primero);
// documento.write(typeof primero);

// // CADENA
// var cadena1 = 'Esto es una cadeba';

// // BOOLEANOS
// var bool = true;
// var bool2 = false;

// // FECHAS
// var fech = Date();
// var fech2 = new Date();

// document.write(typeof primero);
// document.write(fech2.getDay);

// // SIMBOLOS
// var primero = Symbol();

// // OBJETOS


var uno = 20;
var dos = 10;
var tres = ++uno + dos++;

// operadores <, >, <=, >=, ==, !=
// logicos &, &&, ||, |, !

document.write(uno%dos);
document.write(tres);
document.write(uno < dos);

document.write(uno == dos && uno > dos);


